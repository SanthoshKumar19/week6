package com.hcl.lab2;

public class ButtonFactory {
	
	public static Button  getButton(String os) {
		 Button button=null;
		if(os.equalsIgnoreCase("MacOs")) {
			button=new MacOs();
		}
		else if(os.equalsIgnoreCase("WindowsOs")){
			button=new WindowsOs();
		}
		else {
			button =null;
		}
		return button;
	}
}
