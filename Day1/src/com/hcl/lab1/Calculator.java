package com.hcl.lab1;

public class Calculator {
	private static Calculator calci=null;
	private Calculator(){
		
	}
	public static Calculator getCalci() {
		if(calci==null) {
			calci=new Calculator();
		}
		return calci;
	}
	public void add() {
		System.out.println("Adding");
	}
	public void sub() {
		System.out.println("Subtracting");
	}

}
